package cn.edu.bupt.ch9_1_consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ch91ConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Ch91ConsumerApplication.class, args);
    }

}
