package cn.edu.bupt.ch9_1_consumer.Listener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class TopicReceiver {
    @RabbitListener(queues = "beijing")
    public void processBeijing(Map testMessage) {

        System.out.println("Beijing队列消费者收到消息: " + testMessage.toString());
    }

    @RabbitListener(queues = "shanghai")
    public void processShanghai(Map testMessage) {
        System.out.println("Shanghai队列消费者收到消息: " + testMessage.toString());
    }

    @RabbitListener(queues = "news")
    public void processNews(Map testMessage) {
        System.out.println("News队列消费者收到消息: " + testMessage.toString());
    }

    @RabbitListener(queues = "weather")
    public void processWeather(Map testMessage) {
        System.out.println("Weather队列消费者收到消息: " + testMessage.toString());
    }
}
